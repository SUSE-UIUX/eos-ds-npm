## SUSE Design System NPM package

An NPM package to use the SUSE Design System elements with Bootstrap.

## How to use it

Install it with npm
```
npm i --save eos-ds
```

or from Gitlab https://gitlab.com/SUSE-UIUX/eos-ds-npm

You will find a folder called `dist` which has the following structure:

```
- dist/
  - css/
  - images/
  - js/
  - scss/
  - vendors/
  - demo-layout.html
```

### Files included

The `html`, `js`, `css` needed to run the SUSE Design System on your browser are included
in the following folders:

* **css/**
* **js/** Please note that the file `demo.js` isn't needed to run the SUSE Design System.
* **demo-layout.html**

The `/images` folder includes images only used in `demo-layout.html` for [demo purposes](https://suse.eosdesignsystem.com/examples/demo-layout.html).

* **vendors/**: It includes all the dependencies needed to run the SUSE Design System, except for EOS-icons, which you can get from [eos-icons.com](https://eos-icons.com/docs) website.
* **scss/**: It includes all the styles used to define SUSE Design System's structure.

If your project uses SCSS, you can import the `scss/index.scss` file in your project.
Please bear in mind that this file needs to be imported right after Bootstrap's files found in the `/vendors` folder.

```
@import vendors/bootstrap/css/bootstrap.css;
@import scss/index.scss;
```

### Dependencies

SUSE Design System NPM package depends on: Bootstrap, EOS-icons and Jquery.
For the specific version of each dependency, please refer to the [package.json file](https://gitlab.com/SUSE-UIUX/eos/-/raw/master/vendors/package.json) in the EOS repository.

Note: we build our own EOS-icons manually by using the icon picker at https://eos-icons.com
You can use any version of EOS-icons and, naturally, the latest version is recommended!

### Developing the NPM package

Configure `EOS_DS_NPM_URL` in gitlab variables and/or locally to build the package.
It should point to staging (or development if auth is off) in the following format: `EOS_DS_NPM_URL=http://staging-url.com/`

# Learn more about the EOS Design System

* [SUSE Design System](https://productbrand.suse.com)
* [EOS Icons](https://eos-icons.com/)
* [Follow us on Twitter](https://twitter.com/eosdesignsystem)

# Our "thank you" section

## Tested for every browser in every device

Thanks to [Browserstack](https://www.browserstack.com) and their continuous contribution to open source projects, we continuously test the SUSE Design System to make sure all our features and components work perfectly fine in all browsers.
Browserstack helps us make sure our Design System also delivers a peace of mind to all developers and designers making use of our components and layout in their products.
