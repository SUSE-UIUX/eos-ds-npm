#!/bin/bash

## Grab the vendors/package.json file
mkdir -p ./_temp/vendors
wget https://gitlab.com/SUSE-UIUX/eos/-/raw/master/vendors/package.json -q -O ./_temp/vendors/package.json

## I need to get the versions of dependencies first
BOOTSTRAP_VERSION=$(grep -o '"bootstrap": "[^"]*' _temp/vendors/package.json | grep -o '[^"]*$')
JQUERY_VERSION=$(grep -o '"jquery": "[^"]*' _temp/vendors/package.json | grep -o '[^"]*$')

## Replace the versions in our package.json
## In MAC, add '' right after sed -i to test it locally
echo ✅ "writing Bootstrap version $BOOTSTRAP_VERSION in package.json"
sed -i 's/"bootstrap": "[^"]*/"bootstrap": "'$BOOTSTRAP_VERSION'/g' package.json

echo ✅ "writing Jquery version $JQUERY_VERSION in package.json"
sed -i 's/"jquery": "[^"]*/"jquery": "'$JQUERY_VERSION'/g' package.json
